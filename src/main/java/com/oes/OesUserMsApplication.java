package com.oes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OesUserMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OesUserMsApplication.class, args);
	}

}
