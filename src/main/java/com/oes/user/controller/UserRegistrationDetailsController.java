package com.oes.user.controller;

import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oes.commons.BaseDto;
import com.oes.user.model.UserRegistrationDetailsModel;
import com.oes.user.service.UserRegistrationDetailsService;

@RestController
@CrossOrigin

public class UserRegistrationDetailsController {

	@Autowired
	UserRegistrationDetailsService service;
	
	@PostMapping("/RegisterUser")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> saveUser(
			@RequestBody UserRegistrationDetailsModel userDetails) {
		UserRegistrationDetailsModel userDetailsRes = service.saveUser(userDetails);
		return new BaseDto(userDetailsRes, "User Registered Successfully", HttpStatus.OK).respond();
	}

	@GetMapping("/GetByUserId")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> getUserById(@RequestParam Integer userId) {
		UserRegistrationDetailsModel userDetailsRes = service.findUserByUserId(userId);
		return new BaseDto(userDetailsRes, "Query Result Successful", HttpStatus.OK).respond();
	}
	
	@PutMapping("/UpdateUser")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> updateUser(@RequestBody UserRegistrationDetailsModel userDetails) {
		UserRegistrationDetailsModel userDetailsRes = service.updateUser(userDetails);
		return new BaseDto(userDetailsRes, "User Updated Successfully", HttpStatus.OK).respond();
	}

	@GetMapping("/GetByFirstName")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> getFirstName(@RequestParam String firstName) {
		List<UserRegistrationDetailsModel> userDetailsRes = service.findByFirstName(firstName);
		return new BaseDto(userDetailsRes, "Query Result Successful", HttpStatus.OK).respond();
	}
	@GetMapping("/GetByLastName")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> getLastName(@RequestParam String lastName) {
		List<UserRegistrationDetailsModel> userDetailsRes = service.findByLastName(lastName);
		return new BaseDto(userDetailsRes, "Query Result Successful", HttpStatus.OK).respond();
	}
	
	@DeleteMapping("/deleteUser")
	public void deleteUser(@RequestBody UserRegistrationDetailsModel userDetails) {
		service.deleteUser(userDetails);
	}

	@GetMapping("/getUserByUserName")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> getUserByUserName(@RequestParam String userName) {
		UserRegistrationDetailsModel userDetailsRes = service.findUserByUserName(userName);
		return new BaseDto(userDetailsRes, "Query Result Successful", HttpStatus.OK).respond();
	}

	@GetMapping("/getUserByUserNameAndPassword")
	public ResponseEntity<BaseDto<UserRegistrationDetailsModel>> getUserByUserNameAndPassword(
			@RequestParam String userName, @RequestParam String password) {
		UserRegistrationDetailsModel userDetailsRes = service.findUserByUserNameAndPwd(userName, password);
		return new BaseDto(userDetailsRes, "Query Result Successful", HttpStatus.OK).respond();
	}
	
	@GetMapping("/CheckUserName")
	public boolean checkUserName(@RequestParam String value) {
		boolean flag = service.getUserName(value);
		return flag;
	}
	
	@GetMapping("/CheckPhoneNumber")
	public boolean checkPhoneNumber(@RequestParam String value) {
		boolean flag = service.getPhoneNumber(value);
		return flag;
	}

}
