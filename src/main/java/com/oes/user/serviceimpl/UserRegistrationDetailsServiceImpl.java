package com.oes.user.serviceimpl;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.oes.exception.OESException;
import com.oes.user.model.UserRegistrationDetailsModel;
import com.oes.user.repository.UserRegistrationDetailsRepository;
import com.oes.user.service.UserRegistrationDetailsService;

@Service
public class UserRegistrationDetailsServiceImpl implements UserRegistrationDetailsService{

	@Autowired
	UserRegistrationDetailsRepository repo;
	@Override
	public UserRegistrationDetailsModel saveUser(UserRegistrationDetailsModel userRegistrationDetails) {
		
		return repo.save(userRegistrationDetails);	
		}

	@Override
	public UserRegistrationDetailsModel updateUser(UserRegistrationDetailsModel userRegistrationDetails)
	{
		if (Objects.isNull(userRegistrationDetails.getUserRegistrationId()))
		{
			 throw new OESException("invalid update request", HttpStatus.BAD_REQUEST);
			
		}
		return repo.save(userRegistrationDetails);
	}

	@Override
	public void deleteUser(UserRegistrationDetailsModel userRegistrationDetails) {
		// TODO Auto-generated method stub
		repo.delete(userRegistrationDetails);
	}

	@Override
	public UserRegistrationDetailsModel findUserByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return repo.findById(userId).get();
	}

	@Override
	public List<UserRegistrationDetailsModel> findByFirstName(String firstName) {
		// TODO Auto-generated method stub
		return repo.findByFirstName(firstName);
	}
	
	
	@Override
	public List<UserRegistrationDetailsModel> findByLastName(String lastName) {
		// TODO Auto-generated method stub
		return  repo.findByLastName(lastName);
	}
	@Override
	public UserRegistrationDetailsModel findUserByUserName(String userName) {
		// TODO Auto-generated method stub
		return repo.findByEmail(userName);
	}

	@Override
	public UserRegistrationDetailsModel findUserByUserNameAndPwd(String userName, String password) {
		// TODO Auto-generated method stub
		return repo.findByEmailAndPwd(userName,password);
	}
	

	@Override
	public boolean getUserName(String value) {
		List<UserRegistrationDetailsModel> list = repo.findByUserName(value);
		boolean flag = false;
		if(list.size()>0) {
			flag = true;
		}
		return flag;
	}

	@Override
	public boolean getPhoneNumber(String phoneNumber) {
		List<UserRegistrationDetailsModel> list = repo.findByPhoneNo(phoneNumber);
		boolean flag = false;
		if(list.size()>0) {
			flag = true;
		}
		return flag;
	}
	
	

}
