package com.oes.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.oes.user.model.UserRegistrationDetailsModel;

public interface UserRegistrationDetailsRepository extends JpaRepository<UserRegistrationDetailsModel, Integer>
{

	UserRegistrationDetailsModel findByEmail(String userName);
	
	@Query("select r from user_registration_details r where r.email=:userName and r.password=:password")
	UserRegistrationDetailsModel findByEmailAndPwd(@Param("userName") String userName, @Param("password") String password);
	
	@Query("select r from user_registration_details r where r.email=:userName")
	List<UserRegistrationDetailsModel> findByUserName(@Param("userName") String userName);
	
	
	@Query("select r from user_registration_details r where r.firstName=:firstName")
	List<UserRegistrationDetailsModel> findByFirstName(@Param("firstName") String firstName);
	
	List<UserRegistrationDetailsModel> findByPhoneNo(String phoneNo);

	@Query("select r from user_registration_details r where r.lastName=:lastName")
	List<UserRegistrationDetailsModel> findByLastName(@Param("lastName") String lastName);


}
