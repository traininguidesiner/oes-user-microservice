package com.oes.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


public class OESException extends RuntimeException {

	private static final long serialVersionUID = -5948408229365475681L;
	
	private String message;
	
	private Exception exception;
	
	private HttpStatus status;
	
	public OESException(String message, HttpStatus status) {
		this.message = message;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public OESException(String message, Exception exception, HttpStatus status) {
		super();
		this.message = message;
		this.exception = exception;
		this.status = status;
	}

	public OESException() {
		super();
	}
	
	

}
